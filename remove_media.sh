#!/bin/bash

base_path="public/wp-content/uploads"

# Dossier à prendre en compte (ici rangés par années dans WP)
years="2021 2022 2023 2024"

# Extensions des fichiers à supprimer (ici des images)
file_extensions="jpg jpeg png gif svg webp"

# Sauvegarde de la liste des médias supprimé
output_file="listing_removed_media.txt"

> "$output_file"

for year in $years; do
    for ext in $file_extensions; do
        # Simulation (à décommenter si besoin)
        # find "${base_path}/${year}" -type f -iname "*.${ext}" -print >> "$output_file"
        # Suppression définitive
        find "${base_path}/${year}" -type f -iname "*.${ext}" -exec rm -v {} \; >> "$output_file" 2>&1
    done
done

echo "Suppression terminée. Vérifiez le fichier $output_file pour la liste des fichiers trouvés."
