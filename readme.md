# Script de Suppression de Fichiers Média

Ce script bash est conçu pour supprimer automatiquement les fichiers média obsolètes ou inutilisés dans les dossiers spécifiques d'un site WordPress. Il cible les dossiers d'uploads annuels (`2021`, `2022`, `2023`, `2024`) et supprime les fichiers avec les extensions spécifiées.

## Fonctionnalité

Le script parcourt les dossiers `/public/wp-content/uploads` pour les années spécifiées (2021 à 2024) et supprime tous les fichiers ayant les extensions suivantes : `jpg`, `jpeg`, `png`, `gif`, `svg`, `webp`. Une fois la suppression effectuée, le script génère un fichier log listant tous les fichiers qui ont été supprimés.

## Prérequis

- Accès shell à l'environnement de l'hébergeur ou à la machine où les fichiers sont stockés.
- Permissions nécessaires pour supprimer les fichiers dans les répertoires ciblés.

## Utilisation

1. Placez le script dans un emplacement approprié sur votre serveur ou environnement local.
2. Assurez-vous que le script est exécutable : `chmod +x remove_media.sh`.
3. Exécutez le script : `./remove_media.sh`.
4. Consultez le fichier `listing_removed_media.txt` pour vérifier les fichiers qui ont été supprimés.

## Avertissement

**Utilisez ce script avec prudence.** La suppression de fichiers est irréversible. Nous recommandons fortement de réaliser une sauvegarde complète de vos fichiers avant d'exécuter ce script.

## Licence

Ce plugin est distribué sous la licence GPL v2 ou ultérieure.
Tous les autres droits non expressément accordés dans la licence sont réservés.

## Authors

jdesgranges - ** - jdesgranges - **
